//Lets create an Error page, that will display an error message if a client would attempt to go to a route or path that is NOT RECOGNIZED or registered by the app.

import { Container } from 'react-bootstrap';


//create a function that will display an error message page.
//the message that we will display is for us to tell the user that the function/service/page is not found. We need to use the proper HTTP  status code to inform the user better.

export default function ErrorPage() {
	return(
		<Container className="text-center">
			<img  src="https://png.pngtree.com/png-vector/20191004/ourmid/pngtree-sad-icon-isolated-on-abstract-background-png-image_1783237.png"  /> 
			<h1 className="text-center text-danger mt-5"  >404 Page Not Found</h1>
		</Container>
		)
}