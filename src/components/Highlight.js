import { Row, Col, Card } from 'react-bootstrap';


export default function Highlight() {

	return(

		<Row className="mt-3 mb-3">
			{/*1st*/}
			<Col xs={12} md={4}>
				<Card className="
				cardHighlight p-3">
					<Card.Body>
						<Card.Title>Learn From Home</Card.Title>
						<Card.Text>
							"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

		{/*2nd*/}
			<Col xs={12} md={4}>
				<Card className="
				cardHighlight p-3">
					<Card.Body>
						<Card.Title>Study Now, Pay Later</Card.Title>
						<Card.Text>
							"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

		{/*3rd*/}
			<Col xs={12} md={4}>
				<Card className="
				cardHighlight p-3">
					<Card.Body>
						<Card.Title>Be Part of Our Community</Card.Title>
						<Card.Text>
							"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>

		)
}